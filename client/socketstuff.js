let socket;

function socketStuff() {
  socket = io(SOCKETS_URL, { path: SOCKETS_PATH });
  socket.on("roomCode", handleRoomCode);
  socket.on("roomNotFound", handleRoomNotFound);
  socket.on("roomFull", handleRoomFull);
  socket.on("invalidBoardSize", handleInvalidBoardSize);
  socket.on("startGame", handleStartGame);
  socket.on("invalidTurn", handleInvalidTurn);
  socket.on("newTurn", handleNewTurn);
  socket.on("opponentWantsRematch", handleOpponentWantsRematch);
  socket.on("disconnected", handleDisconnected);
}

function handleRoomCode(roomCode) {
  startScreenElement.classList.add("hidden");
  waitingScreenElement.classList.remove("hidden");
  roomCodeElement.innerHTML = roomCode;
  roomCodeLinkElement.value = window.location.href.split(/[?#]/)[0] + "?room=" + roomCode;
}

function handleRoomNotFound(roomCode) {
  alert("Room with code " + roomCode + " not found.");
}

function handleRoomFull(roomCode) {
  alert("Room with code " + roomCode + " is full!");
}

function handleInvalidBoardSize(boardSize) {
  alert("Invalid board size: " + boardSize);
}

function handleStartGame(playerNumberText, gameStateJSON) {
  playerNumber = Number(playerNumberText);
  gameState = JSON.parse(gameStateJSON);

  canvas.width = GAME_WIDTH;
  canvas.height = gameState.boardHeight;
  canvas.onclick = handleClick;

  startScreenElement.classList.add("hidden");
  waitingScreenElement.classList.add("hidden");
  gameScreenElement.classList.remove("hidden");

  resultMessageElement.classList.add("hidden");
  turnMessageElement.classList.remove("hidden");

  rematchButtonElement.classList.add("hidden");
  rematchTextElement.classList.add("hidden");

  leaveRoomButtonElement2.classList.add("hidden");

  clearInterval(highlightInterval);
  highlightInterval = setInterval(() => {
    shouldHighlight = !shouldHighlight;
    drawBoard();
  }, FLASH_TIMEOUT);

  boardGap = gameState.boardHeight / gameState.boardSize;
  boardWidth = boardGap / BOARD_SLANT / GAP_RATIO;
  board_x = (GAME_WIDTH - boardWidth - boardGap / GAP_RATIO) / 2;
  board_y = BOARD_PADDING;
  drawGame();
}

function handleInvalidTurn() {
  alert("Invalid turn.");
  drawGame();
}

function handleNewTurn(gameStateJSON) {
  gameState = JSON.parse(gameStateJSON);
  drawGame();
  if (gameState.winner !== 0) {
    drawGameOver();
  }
}

function handleOpponentWantsRematch() {
  rematchTextElement.innerHTML = "Your opponent wants a rematch.";
  rematchTextElement.classList.remove("hidden");
}

function handleDisconnected() {
  rematchTextElement.innerHTML = "Your opponent has disconnected.";
  rematchTextElement.classList.remove("hidden");
  rematchButtonElement.classList.add("hidden");
  // makeTurnButtonElement.classList.add("hidden");
  leaveRoomButtonElement2.classList.remove("hidden");
}

function emitNewRoom() {
  let size = boardSizeInputElements[0].value;
  if (size > 0) {
    socket.emit("newRoom", size);
  } else {
    alert("Invalid size!");
  }
}

function emitJoinRoom(roomCode) {
  if (roomCode) {
    socket.emit("joinRoom", roomCode.toUpperCase());
    roomCodeInputElement.value = "";
  }
}

function emitJoinRoomWithURLCode() {
  const urlParams = new URLSearchParams(window.location.search);
  const roomCode = urlParams.get("room");
  if (roomCode) {
    history.replaceState(null, "", ROOT_PATH); // remove room code parameter from URL
    emitJoinRoom(roomCode);
  }
}

function emitMakeTurn() {
  socket.emit("makeTurn", JSON.stringify(selectedSquare));
}

function emitRematch() {
  rematchTextElement.innerHTML = "Requested rematch.";
  rematchTextElement.classList.remove("hidden");
  rematchButtonElement.classList.add("hidden");
  socket.emit("rematch");
}

function emitLeaveRoom() {
  startScreenElement.classList.remove("hidden");
  waitingScreenElement.classList.add("hidden");
  gameScreenElement.classList.add("hidden");

  socket.emit("leaveRoom");
}
