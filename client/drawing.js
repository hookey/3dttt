function drawGame() {
  selectedSquare = [];

  drawTurn();
  drawBoard();
  drawActions();
}

function drawGameOver() {
  resultMessageElement.innerHTML = gameState.winner === playerNumber ? "You win!" : "You lose!";
  resultMessageElement.classList.remove("hidden");
  turnMessageElement.classList.add("hidden");
  rematchButtonElement.classList.remove("hidden");
  leaveRoomButtonElement2.classList.remove("hidden");
}

function drawTurn() {
  let turnColor = gameState.currentPlayer == 1 ? "blue" : "red";
  whoseTurnElement.innerHTML = playerNumber == gameState.currentPlayer ? "your" : "your opponent's";
  whoseTurnElement.classList.remove("red");
  whoseTurnElement.classList.remove("blue");
  whoseTurnElement.classList.add(turnColor);
}

function drawActions() {
  if (gameState.currentPlayer !== playerNumber) {
    notSelectedTextElement.classList.add("hidden");
    makeTurnButtonElement.classList.add("hidden");
  } else if (selectedSquare.length > 0) {
    notSelectedTextElement.classList.add("hidden");
    makeTurnButtonElement.classList.remove("hidden");
  } else {
    notSelectedTextElement.classList.remove("hidden");
    makeTurnButtonElement.classList.add("hidden");
  }
}

function drawBoard() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  for (let z = 0; z < gameState.boardSize; z++) {
    for (let y = 0; y < gameState.boardSize; y++) {
      for (let x = 0; x < gameState.boardSize; x++) {
        let squarePlayer = getSquare(gameState.board, [x, y, z]);
        let boardColor = BOARD_COLOR;
        let squareColor;
        if (sameSquare(selectedSquare, [x, y, z])) {
          squareColor = SELECT_COLOR;
        }
        if (squarePlayer === 1) {
          squareColor = PLAYER1_COLOR;
        } else if (squarePlayer === 2) {
          squareColor = PLAYER2_COLOR;
        }
        if (shouldHighlight) {
          if (gameState.highlights.some((highlight) => sameSquare(highlight, [x, y, z]))) {
            squareColor = squarePlayer == 1 ? PLAYER1_HIGHLIGHT : PLAYER2_HIGHLIGHT;
          }
        }
        let [square_x, square_y] = gameSpaceToBoardSpace(board_x, board_y, x, y, z);
        drawSquare(
          ctx,
          boardColor,
          square_x,
          square_y,
          boardWidth / gameState.boardSize,
          BOARD_SLANT,
          squareColor
        );
      }
    }
  }
}

function drawSquare(ctx, color, x, y, width, slant, fillColor) {
  ctx.beginPath();
  ctx.strokeStyle = color;
  ctx.lineWidth = 2;

  ctx.moveTo(x, y);
  ctx.lineTo(x + width, y);
  ctx.lineTo(x + width + slant * width, y + slant * width);
  ctx.lineTo(x + slant * width, y + slant * width);
  ctx.lineTo(x, y);
  ctx.stroke();

  if (fillColor) {
    ctx.fillStyle = fillColor;
    ctx.fill();
  }
}

function gameSpaceToBoardSpace(start_x, start_y, game_x, game_y, game_z) {
  boardSpace_x =
    start_x +
    game_x * (boardWidth / gameState.boardSize) +
    game_y * BOARD_SLANT * (boardWidth / gameState.boardSize);
  boardSpace_y =
    start_y + game_y * (BOARD_SLANT * (boardWidth / gameState.boardSize)) + game_z * boardGap;
  return [boardSpace_x, boardSpace_y];
}

function boardSpaceToGameSpace(start_x, start_y, boardSpace_x, boardSpace_y) {
  // Canvas gets smaller on mobile devies, but click coordinates stay the same
  // Need to apply the scale to the board space coordinates with the scale
  let canvasScale = canvas.clientWidth / GAME_WIDTH;
  [boardSpace_x, boardSpace_y] = [
    boardSpace_x / canvasScale - start_x,
    boardSpace_y / canvasScale - start_y,
  ];
  z = Math.floor(boardSpace_y / boardGap);
  y = Math.floor(
    (boardSpace_y - z * boardGap) / ((boardWidth * BOARD_SLANT) / gameState.boardSize)
  );
  x = Math.floor(
    (boardSpace_x - (boardSpace_y - z * boardGap)) / (boardWidth / gameState.boardSize)
  );
  return [x, y, z];
}

function validateCoordinates(x, y, z, boardSize) {
  for (let coordinate of [x, y, z]) {
    if (coordinate < 0 || coordinate >= boardSize) {
      return false;
    }
  }
  return true;
}

function handleClick(e) {
  if (gameState.currentPlayer !== playerNumber) {
    return;
  }

  let [x, y, z] = boardSpaceToGameSpace(board_x, board_y, e.offsetX, e.offsetY);
  if (validateCoordinates(x, y, z, gameState.boardSize) && gameState.board[z][y][x] === 0) {
    selectedSquare = [x, y, z];
  } else {
    selectedSquare = [];
  }

  drawBoard();
  drawActions();
}

function getSquare(board, coords) {
  return board[coords[2]][coords[1]][coords[0]];
}

function sameSquare(square1, square2) {
  return [0, 1, 2].every((i) => square1[i] === square2[i]);
}
