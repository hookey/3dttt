let boardGap, boardWidth, board_x, board_y, currentGameWidth;

let oldGameState;
let currentPlayer;
let canvas, ctx;
let selectedSquare;
let shouldHighlight = false;
let highlightInterval;

let playerNumber;
let gameState;

let startScreenElement,
  boardSizeInputElements,
  createRoomButtonElement,
  roomCodeInputElement,
  joinRoomButtonElement,
  waitingScreenElement,
  roomCodeElement,
  roomCodeLinkElement,
  gameScreenElement,
  turnMessageElement,
  resultMessageElement,
  whoseTurnElement,
  notSelectedTextElement,
  makeTurnButtonElement,
  rematchButtonElement,
  rematchTextElement,
  leaveRoomButtonElement1,
  leaveRoomButtonElement2;

function init() {
  socketStuff();
  initElements();
  emitJoinRoomWithURLCode();
}
window.onload = init;

function initElements() {
  startScreenElement = document.getElementById("startScreen");
  boardSizeInputElements = document.getElementsByClassName("boardSizeInput");
  createRoomButtonElement = document.getElementById("createRoomButton");
  roomCodeInputElement = document.getElementById("roomCodeInput");
  joinRoomButtonElement = document.getElementById("joinRoomButton");

  waitingScreenElement = document.getElementById("waitingScreen");
  roomCodeElement = document.getElementById("roomCode");
  roomCodeLinkElement = document.getElementById("roomCodeLink");

  gameScreenElement = document.getElementById("gameScreen");
  turnMessageElement = document.getElementById("turnMessage");
  resultMessageElement = document.getElementById("resultMessage");
  whoseTurnElement = document.getElementById("whoseTurn");
  makeTurnButtonElement = document.getElementById("makeTurnButton");
  notSelectedTextElement = document.getElementById("notSelectedText");
  rematchButtonElement = document.getElementById("rematchButton");
  rematchTextElement = document.getElementById("rematchText");
  leaveRoomButtonElement1 = document.getElementById("leaveRoomButton1");
  leaveRoomButtonElement2 = document.getElementById("leaveRoomButton2");

  canvas = document.getElementById("game");
  ctx = canvas.getContext("2d");

  for (let boardSizeInputElement of boardSizeInputElements) {
    boardSizeInputElement.oninput = () => {
      for (let otherBoardSizeInputElement of boardSizeInputElements) {
        otherBoardSizeInputElement.value = boardSizeInputElement.value;
      }
    };
  }

  createRoomButtonElement.onclick = emitNewRoom;
  joinRoomButtonElement.onclick = () => emitJoinRoom(roomCodeInputElement.value);
  makeTurnButtonElement.onclick = emitMakeTurn;
  rematchButtonElement.onclick = emitRematch;
  leaveRoomButtonElement1.onclick = emitLeaveRoom;
  leaveRoomButtonElement2.onclick = emitLeaveRoom;
}
