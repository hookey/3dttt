# 3D Tic-Tac-Toe

Play 3D Tic-Tac-Toe online with a friend. Supports board sizes from 1x1x1 to 10x10x10. The only one worth playing is probably 4x4x4 though.

Running at https://andri.io/3dttt/.

Server: Node.js, socket.io

Client: VanillaJS, socket.io

## Server setup

In the `server/` directory, run `npm install` or `yarn install` and then `node server.js`.

## Client setup

Change `SOCKETS_URL` and `SOCKETS_PATH` in `constants.js`. Then just open `index.html`.
