const io = require("socket.io")({
  path: "/3dttt/socketio",
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

const port = process.env.NODE_PORT || 3000;
const { generateID } = require("./utils");
const { newGameState, makeTurn } = require("./game");

let gameStates = {};
let clientRooms = {};

io.on("connection", (client) => {
  client.on("newRoom", handleNewRoom);
  client.on("joinRoom", handleJoinRoom);
  client.on("makeTurn", handleMakeTurn);
  client.on("rematch", handleRematch);
  client.on("leaveRoom", handleLeaveRoom);
  client.on("disconnect", handleLeaveRoom);

  function handleNewRoom(boardSizeText) {
    let boardSize = Math.round(Number(boardSizeText));
    if (isNaN(boardSize) || boardSize < 1 || boardSize > 10) {
      client.emit("invalidBoardSize", boardSizeText);
      return;
    }

    let roomCode;
    while (roomCode == null || roomCode in clientRooms) {
      roomCode = generateID(4);
    }
    //roomCode = "TEST";
    clientRooms[client.id] = roomCode;
    client.emit("roomCode", roomCode);

    gameStates[roomCode] = newGameState(boardSize);
    client.join(roomCode);
    client.playerNumber = 1;
  }

  function handleJoinRoom(roomCode) {
    roomCode = roomCode.toUpperCase();
    const room = io.sockets.adapter.rooms.get(roomCode);

    let numClients = room ? room.size : 0;

    if (numClients === 0) {
      client.emit("roomNotFound", roomCode);
      return;
    } else if (numClients > 1) {
      client.emit("roomFull", roomCode);
      return;
    }

    clientRooms[client.id] = roomCode;
    client.join(roomCode);
    client.playerNumber = 2;

    emitStartGame(roomCode);
  }

  function handleMakeTurn(selectedSquareJSON) {
    const selectedSquare = JSON.parse(selectedSquareJSON);
    const roomCode = clientRooms[client.id];
    const gameState = gameStates[roomCode];

    // If player sends makeTurn on opponent's turn
    if (gameState.currentPlayer !== client.playerNumber) {
      client.emit("invalidTurn");
    }

    let newGameState = makeTurn(gameState, selectedSquare, client.playerNumber);

    // If selected square was not 0
    if (!newGameState) {
      client.emit("invalidTurn");
    }

    io.sockets.in(roomCode).emit("newTurn", JSON.stringify(newGameState));
  }

  function handleRematch() {
    client.rematch = true;
    const roomCode = clientRooms[client.id];

    const clientIds = io.sockets.adapter.rooms.get(roomCode);
    if (
      [...clientIds].every((clientId) => {
        const clientSocket = io.sockets.sockets.get(clientId);
        if (clientSocket.rematch) {
          return true;
        }
        clientSocket.emit("opponentWantsRematch");
      })
    ) {
      let oldWinner = gameStates[roomCode].winner;
      gameStates[roomCode] = newGameState(gameStates[roomCode].boardSize);
      gameStates[roomCode].currentPlayer = 1 + (oldWinner % 2);
      emitStartGame(clientRooms[client.id]);
    }
  }

  function handleLeaveRoom() {
    let roomCode = clientRooms[client.id];
    client.leave(roomCode);

    io.sockets.in(roomCode).emit("disconnected", client.playerNumber);
  }
});

function emitStartGame(roomCode) {
  for (let clientId of io.sockets.adapter.rooms.get(roomCode)) {
    const clientSocket = io.sockets.sockets.get(clientId);
    clientSocket.rematch = false;
    clientSocket.emit("startGame", clientSocket.playerNumber, JSON.stringify(gameStates[roomCode]));
  }
}

io.listen(port);
