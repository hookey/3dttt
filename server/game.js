module.exports = {
  newGameState,
  makeTurn,
};

const ROW_HEIGHT = 160;

function newGameState(boardSize) {
  let gameState = {
    board: [],
    boardSize: boardSize,
    winner: 0,
    currentPlayer: 1 + Math.floor(Math.random() * 2),
    highlights: [],
    boardHeight: ROW_HEIGHT * boardSize,
  };
  for (let z = 0; z < boardSize; z++) {
    gameState.board.push([]);
    for (let y = 0; y < boardSize; y++) {
      gameState.board[z].push([]);
      for (let x = 0; x < boardSize; x++) {
        gameState.board[z][y].push(0);
      }
    }
  }
  return gameState;
}

function makeTurn(gameState, selectedSquare, playerNumber) {
  let square = getSquare(gameState.board, selectedSquare);
  if (square !== 0) {
    return null;
  }

  gameState.board = setSquare(gameState.board, selectedSquare, playerNumber);
  gameState.highlights = [selectedSquare];
  gameState.currentPlayer = 1 + (playerNumber % 2);

  let winners = checkWin(gameState);
  if (winners.length > 0) {
    gameState.highlights = [];
    for (let winner of winners) {
      gameState.winner = winner.winner;
      gameState.currentPlayer = 0;
      gameState.highlights.push(...winner.squares);
    }
  }

  return gameState;
}

function getSquare(board, coords) {
  if (board[coords[2]]) {
    if (board[coords[2]][coords[1]]) {
      return board[coords[2]][coords[1]][coords[0]];
    }
  }
  return undefined;
}

function setSquare(board, coords, value) {
  board[coords[2]][coords[1]][coords[0]] = value;
  return board;
}

function checkWin(gameState) {
  let lines = [];
  // tic-tac-toe lines from the z-axis (top-down)
  for (let z = 0; z < gameState.boardSize; z++) {
    for (let i = 0; i < gameState.boardSize; i++) {
      // rows
      let row = [];
      for (let x = 0; x < gameState.boardSize; x++) {
        row.push([x, i, z]);
      }
      lines.push(row);
      // columns
      let column = [];
      for (let y = 0; y < gameState.boardSize; y++) {
        column.push([i, y, z]);
      }
      lines.push(column);
    }
    // diagonals
    let diagonals = [[], []];
    for (let i = 0; i < gameState.boardSize; i++) {
      diagonals[0].push([i, i, z]);
      diagonals[1].push([gameState.boardSize - 1 - i, i, z]);
    }
    lines.push(...diagonals);
  }
  // tic-tac-toe lines from the y-axis (from the side)
  // rows have already been checked
  for (let y = 0; y < gameState.boardSize; y++) {
    for (let x = 0; x < gameState.boardSize; x++) {
      // columns
      let column = [];
      for (let z = 0; z < gameState.boardSize; z++) {
        column.push([x, y, z]);
      }
      lines.push(column);
    }
    // diagonals
    let diagonals = [[], []];
    for (let i = 0; i < gameState.boardSize; i++) {
      diagonals[0].push([i, y, i]);
      diagonals[1].push([gameState.boardSize - 1 - i, y, i]);
    }
    lines.push(...diagonals);
  }
  // tic-tac-toe lines from the x-axis (from the third side)
  // rows and columns have already been checked
  for (let x = 0; x < gameState.boardSize; x++) {
    // diagonals
    let diagonals = [[], []];
    for (let i = 0; i < gameState.boardSize; i++) {
      diagonals[0].push([x, i, i]);
      diagonals[1].push([x, gameState.boardSize - 1 - i, i]);
    }
    lines.push(...diagonals);
  }
  // the four 3D diagonals
  let diagonals = [[], [], [], []];
  for (let i = 0; i < gameState.boardSize; i++) {
    diagonals[0].push([i, i, i]);
    diagonals[1].push([gameState.boardSize - 1 - i, gameState.boardSize - 1 - i, i]);
    diagonals[2].push([gameState.boardSize - 1 - i, i, i]);
    diagonals[3].push([i, gameState.boardSize - 1 - i, i]);
  }
  lines.push(...diagonals);

  let winners = [];
  for (let player of [1, 2]) {
    for (let line of lines) {
      if (line.every((c) => getSquare(gameState.board, c) === player)) {
        winners.push({
          winner: player,
          squares: line,
        });
      }
    }
  }

  return winners;
}
